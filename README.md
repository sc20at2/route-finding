# route-finding

### Rquirements folder contains the task requirements, data & planning report

### Implementation

#### dijkstra.h - methods signatures for shortest path part of the project
#### parser.h - methods signatures for parsing/processing the input data
#### visualizer.h - methods signatures for visualisation of the data

#### main.c entry point for the project

## how to run

### ensure you have gnuplot installed on your machine
### run
    make
### execute
    ./main


##Behavior

    The program will compute the shortest path between start node (0 as default, can be changed to any other) using dijkstra algorithm. 
    The output with shortest path to all the edges will be printed to shortest_paths.out file. 
    Gnuplot was used for plotting data. 


url: https://gitlab.com/sc20at2/route-finding/-/tree/master
![img.png](img.png)


##Result
![img_1.png](img_1.png)