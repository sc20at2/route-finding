typedef enum { false, true } bool;

struct Node {
    int id;
    int order_id;
    double lat;
    double lon;
};

struct Link {
    int nodeA;
    int nodeB;
    double length;
};

// A node in adjacency list
struct AdjListNode{
    int dest;
    int weight;
    struct AdjListNode* next;
};

// An adjacency list
struct AdjList{
    struct AdjListNode *head;
};

// A graph, V = number of nodes
struct Graph{
    int V;
    struct AdjList* array;
};

struct AdjListNode* newAdjListNode(
        int dest, int weight);

struct Graph* createGraph(int V);

void addEdge(struct Graph* graph, int src,
             int dest, double weight);

struct MinHeapNode{
    int  v;
    int dist;
};

struct MinHeap{

    // Number of heap nodes present currently
    int size;

    // Capacity of min heap
    int capacity;

    int *pos;
    struct MinHeapNode **array;
};


struct MinHeapNode* newMinHeapNode(int v,
                                   int dist);

struct MinHeap* createMinHeap(int capacity);


void swapMinHeapNode(struct MinHeapNode** a,
                     struct MinHeapNode** b);

void minHeapify(struct MinHeap* minHeap,
                int idx);

int isEmpty(struct MinHeap* minHeap);

struct MinHeapNode* extractMin(struct MinHeap*
minHeap);

void decreaseKey(struct MinHeap* minHeap,
                 int v, int dist);

bool isInMinHeap(struct MinHeap *minHeap, int v);

void printArr(int src, int dist[], int parent[], int n);

void printPath(FILE *fp, int parent[], int j);
void printPathWithLatLon(FILE *fp, int parent[], int j, struct Node inputNodes[]);
void dijkstra(struct Graph* graph, int src, int dist[], int parent[]);

