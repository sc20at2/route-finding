#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "dijkstra.h"

// creates a new adjacency list node
struct AdjListNode* newAdjListNode(
        int dest, int weight) {

    struct AdjListNode* newNode =
            (struct AdjListNode*)
                    malloc(sizeof(struct AdjListNode));

    newNode->dest = dest;
    newNode->weight = weight;
    newNode->next = NULL;

    return newNode;
}

// creates a graph with V being number of vertices
struct Graph* createGraph(int V){
    struct Graph* graph = (struct Graph*)
            malloc(sizeof(struct Graph));
    graph->V = V;

    // create an array of adjacency lists with size equal to number of vertices.
    graph->array = (struct AdjList*)
            malloc(V * sizeof(struct AdjList));

    // head is NULL -> adjacency list is empty
    for (int i = 0; i < V; ++i)
        graph->array[i].head = NULL;

    return graph;
}

// Adds an edge to the graph
void addEdge(struct Graph* graph, int src,
             int dest, double weight){

    //We have an undirected graph.

    //Add edge from src to dest.
    struct AdjListNode* newNode =
            newAdjListNode(dest, weight);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;

    // add edge from dest to src
    newNode = newAdjListNode(src, weight);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

// A utility function to create a Min Heap Node
struct MinHeapNode* newMinHeapNode(int v,
                                   int dist){
    struct MinHeapNode* minHeapNode =
            (struct MinHeapNode*)
                    malloc(sizeof(struct MinHeapNode));
    minHeapNode->v = v;
    minHeapNode->dist = dist;

    return minHeapNode;
}

// A utility function to create a Min Heap
struct MinHeap* createMinHeap(int capacity){
    struct MinHeap* minHeap =
            (struct MinHeap*)
                    malloc(sizeof(struct MinHeap));
    minHeap->pos = (int *)malloc(
            capacity * sizeof(int));

    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array =
            (struct MinHeapNode**)
                    malloc(capacity *
                           sizeof(struct MinHeapNode*));

    return minHeap;
}

// A utility function to swap two
// nodes of min heap
void swapMinHeapNode(struct MinHeapNode** a,
                     struct MinHeapNode** b){
    struct MinHeapNode* t = *a;

    *a = *b;
    *b = t;
}

// heapify the minHeap at given idx
void minHeapify(struct MinHeap* minHeap, int idx){
    int smallest, left, right;
    smallest = idx;
    left = 2 * idx + 1;
    right = 2 * idx + 2;

    if (left < minHeap->size &&
        minHeap->array[left]->dist <
        minHeap->array[smallest]->dist )
        smallest = left;

    if (right < minHeap->size &&
        minHeap->array[right]->dist <
        minHeap->array[smallest]->dist )
        smallest = right;

    if (smallest != idx)
    {
        // The nodes to be swapped in min heap
        struct MinHeapNode *smallestNode =
                minHeap->array[smallest];
        struct MinHeapNode *idxNode =
                minHeap->array[idx];

        // Swap positions
        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v] = smallest;

        // Swap nodes
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);

        minHeapify(minHeap, smallest);
    }
}

// check if given minHeap is empty
int isEmpty(struct MinHeap* minHeap){
    return minHeap->size == 0;
}

// pop (top) minimum from minHeap
struct MinHeapNode* extractMin(struct MinHeap*
minHeap){
    if (isEmpty(minHeap))
        return NULL;

    // Store the root node
    struct MinHeapNode* root =
            minHeap->array[0];

    // Replace root node with last node
    struct MinHeapNode* lastNode =
            minHeap->array[minHeap->size - 1];
    minHeap->array[0] = lastNode;

    // Update position of last node
    minHeap->pos[root->v] = minHeap->size-1;
    minHeap->pos[lastNode->v] = 0;

    // Reduce heap size and heapify root
    --minHeap->size;
    minHeapify(minHeap, 0);

    return root;
}

// decrease dist value of a given vertex v.
// Use pos[] of min heap to get the current index of node in min heap
void decreaseKey(struct MinHeap* minHeap,
                 int v, int dist){

    // Get the index of v in heap array
    int i = minHeap->pos[v];

    // Get the node and update its dist value
    minHeap->array[i]->dist = dist;

    // Travel up while the complete
    // tree is not heapified.

    while (i && minHeap->array[i]->dist <
                minHeap->array[(i - 1) / 2]->dist){
        // Swap this node with its parent
        minHeap->pos[minHeap->array[i]->v] =
                (i-1)/2;
        minHeap->pos[minHeap->array[
                (i-1)/2]->v] = i;
        swapMinHeapNode(&minHeap->array[i],
                        &minHeap->array[(i - 1) / 2]);

        // move to parent index
        i = (i - 1) / 2;
    }
}

// Check if a given vertex exists in minHeap
bool isInMinHeap(struct MinHeap *minHeap, int v){
    if (minHeap->pos[v] < minHeap->size)
        return true;
    return false;
}

void printPathWithLatLon(FILE *fp, int parent[], int j, struct Node inputNodes[]){

    // If j is source
    if (parent[j] == - 1)
        return;

    printPathWithLatLon(fp, parent, parent[j], inputNodes);

    fprintf(fp, "%lf %lf\n", inputNodes[j].lon, inputNodes[j].lat);
}

// Print shortest
// path from source to j
// using parent array
void printPath(FILE *fp, int parent[], int j){

    // If j is source
    if (parent[j] == - 1)
        return;

    printPath(fp, parent, parent[j]);

    fprintf(fp, "%d ", j);
}

// Print output to file
void printArr(int src, int dist[], int parent[], int n){
    FILE *fp;

    fp = fopen("shortest_paths.out", "w");

    if(fp == NULL) {
        return;
    }

    for (int i = 0; i < n; ++i) {
        fprintf(fp, "from [ %d ] -> [ %d ], shortest path sum = [ %d ] is through nodes [ ", src, i, dist[i]);
        printPath(fp, parent, i);
        fprintf(fp, "]\n");
    }

    fclose(fp);
}


// Dijkstra algorithm for finding shortes paths from source to every node.
// It is a O(ELogV) function
void dijkstra(struct Graph* graph, int src, int dist[], int parent[]){

    int V = graph->V;

    // minHeap represents set E
    struct MinHeap* minHeap = createMinHeap(V);

    // Initialize min heap with all
    // vertices. dist value of all vertices
    for (int v = 0; v < V; ++v){
        dist[v] = INT_MAX;
        parent[0] = -1;
        minHeap->array[v] = newMinHeapNode(v, dist[v]);
        minHeap->pos[v] = v;
    }

    // Make dist value of src vertex
    // as 0 so that it is extracted first
    minHeap->array[src] =
            newMinHeapNode(src, dist[src]);
    minHeap->pos[src]   = src;
    dist[src] = 0;
    decreaseKey(minHeap, src, dist[src]);

    // Initially size of min heap is equal to V
    minHeap->size = V;

    while (!isEmpty(minHeap)){

        // Extract the node with
        // minimum distance value
        struct MinHeapNode* minHeapNode =
                extractMin(minHeap);

        // Store the extracted node order id
        int u = minHeapNode->v;

        // Traverse through all adjacent
        // vertices of u and update
        // their distance values
        struct AdjListNode* pCrawl =
                graph->array[u].head;

        while (pCrawl != NULL){
            int v = pCrawl->dest;

            if (isInMinHeap(minHeap, v) &&
                dist[u] != INT_MAX &&
                pCrawl->weight + dist[u] < dist[v]){
                dist[v] = dist[u] + pCrawl->weight;
                parent[v] = u;

                // update distance and value in minheap
                decreaseKey(minHeap, v, dist[v]);
            }
            pCrawl = pCrawl->next;
        }
    }
}
