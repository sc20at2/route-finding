#include <string.h>
#include "parser.h"
#include <stdlib.h>

int is_link(char* line) {
    char start[6];
    char linkTag[6] = "<link\0";

    strncpy(start, line, 5);

    return strcmp(start, linkTag) == 0;
}

int is_node(char* line) {
    char start[6];
    char nodeTag[6] = "<node";

    strncpy(start, line, 5);

    return strcmp(start, nodeTag) == 0;
}


int parse_link(char* line, int* nodeA, int* nodeB, double* length) {
    bool first = false;

    // Extract the first token
    char * token = strtok(line, " ");

    // loop through the string to extract all other tokens
    while( token != NULL ) {
        if(strstr(token, "node")) {
            char attr[20];

            strcpy(attr, token + 5);
            if(!first) {
                *nodeA = atoi(attr);
                first = true;
            } else {
                *nodeB = atoi(attr);
            }
        } else if (strstr(token, "length")) {
            char attr[20];
            strcpy(attr, token + 7);
            *length = atof(attr);
        }
        token = strtok(NULL, " ");
    }

    return 0;
}

int parse_node(char* line, int* id, double* lat, double* lon) {

    // Extract the first token
    char * token = strtok(line, " ");

    // loop through the string to extract all other tokens
    while( token != NULL ) {
        if(strstr(token, "lon")){
            char attr[20];
            strcpy(attr, token + 4);

            *lon = atof(attr);
        } else if(strstr(token, "lat")) {
            char attr[20];
            strcpy(attr, token + 4);

            *lat = atof(attr);
        } else if(strstr(token, "id")) {
            char attr[20];
            strcpy(attr, token + 3);

            *id = atoi(attr);
        }

        token = strtok(NULL, " ");
    }

    return 0;
}

int parse_file(int* nodes, int* links, struct Node inputNodes[], struct Link inputLinks[]) {
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("input.map", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    //Link
    int nodeA;
    int nodeB;
    double length;

    //Node
    int id;
    double lat;
    double lon;

    while ((read = getline(&line, &len, fp)) != -1) {

        if (is_link(line)) {
            struct Link currentLink;

            parse_link(line, &nodeA, &nodeB, &length);

            currentLink.nodeA = nodeA;
            currentLink.nodeB = nodeB;
            currentLink.length = length;

            inputLinks[(*links)++] = currentLink;
        }

        if (is_node(line)) {
            struct Node currentNode;

            parse_node(line, &id, &lat, &lon);

            currentNode.id = id;
            currentNode.lat = lat;
            currentNode.lon = lon;

            inputNodes[(*nodes)++] = currentNode;
        }

    }

    fclose(fp);

    if (line) {
        free(line);
    }

    return 0;
}