#include <stdio.h>
#include <stdlib.h>
#include "dijkstra.h"

int is_link(char* line);

int is_node(char* line);

int parse_link(char* line, int* nodeA, int* nodeB, double* length);

int parse_node(char* line, int* id, double* lat, double* lon);

int parse_file(int* nodes, int* links, struct Node inputNodes[], struct Link inputLinks[]);