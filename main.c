#define NUM_COMMANDS 2
#include "parser.h"


int compare (const void * a, const void * b){

    struct Node *nodeA = (struct Node *)a;
    struct Node *nodeB = (struct Node *)b;

    if (nodeA->id > nodeB->id) {
        return 1;
    }

    return -1;
}

////TODO binary search
//int get_idx(int nodes, struct Node inputNodes[], int id) {
//    for(int i=0;i<nodes;i++){
//        if(inputNodes[i].id == id) {
//            return inputNodes[i].order_id;
//        }
//    }
//
//    return -1;
//}

int binary_search(struct Node inputNodes[], int l, int r, int id){
    if (r >= l) {
        int mid = l + (r - l) / 2;

        // If the element is present at the middle, return it
        if (inputNodes[mid].id == id)
            return mid;

        // If element is smaller than mid, then it should be in the left
        if (inputNodes[mid].id > id)
            return binary_search(inputNodes, l, mid - 1, id);

        // Else the element can only be in the right
        return binary_search(inputNodes, mid + 1, r, id);
    }

    // Element wasn't found
    return -1;
}


void render(int nodes, int links, struct Node inputNodes[], struct Link inputLinks[]) {
    char * commandsForGnuplot[] = {"set title \"Graph nodes plotting\"", "plot 'data.temp'"};

    FILE * temp = fopen("data.temp", "w");

    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");

    for (int i=0; i < nodes; i++){
        fprintf(temp, "%lf %lf %d\n", inputNodes[i].lat, inputNodes[i].lon, inputNodes[i].order_id);
    }

    for (int i=0; i < NUM_COMMANDS; i++){
        fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]);
    }
}


void render_shortest(int src, int dist[], int parent[], int nodes, int links, struct Node inputNodes[], struct Link inputLinks[]){
    char * commandsForGnuplot[] = {"set title \"Shortest paths from src to 15 random other nodes\"", "plot 'shortest.temp' u 1:2:(0.0001) with circles linecolor rgb \"white\" lw 2 fill solid border lc lt 0 notitle, 'shortest.temp' u 1:2 with lines lc rgb \"black\" lw 2 notitle"};


        FILE * temp = fopen("shortest.temp", "w");

    for (int i = 0; i < 15; ++i) {
        printPathWithLatLon(temp, parent, i, inputNodes);
        fprintf(temp, "\n");
    }

    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");

    for (int i=0; i < NUM_COMMANDS; i++){
        fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]);
    }
}


int main() {

    int nodes = 0;
    int links = 0;
    struct Node inputNodes[5000];
    struct Link inputLinks[5000];

    parse_file(&nodes, &links, inputNodes, inputLinks);

    qsort(inputNodes, nodes, sizeof(struct Node), compare);

    for(int i=0; i<nodes; i++){
        inputNodes[i].order_id = i;
    }

    int V = nodes;

    // dist values used to pick
    // minimum weight edge in cut
    int dist[V];

    // store shortest path back traversal
    int parent[V];

    struct Graph* graph = createGraph(V);

    printf("Number of links: [ %d ]\n", links);

//    for(int i=0; i<links; i++){
//        printf(" [ %d ] -> [ %d ] = [ %f ]\n", inputLinks[i].nodeA, inputLinks[i].nodeB, inputLinks[i].length);
//    }

    printf("Number of nodes: [ %d ]\n", nodes);
//    for(int i=0; i<5; i++){
//        printf(" [ %d ] : [ %d ] = [ %f ] [ %f ]\n", inputNodes[i].id, inputNodes[i].order_id, inputNodes[i].lat, inputNodes[i].lon);
//    }

    for(int i=0; i<links; i++){
        int nodeAIdx = binary_search(inputNodes, 0, nodes - 1, inputLinks[i].nodeA);
        int nodeBIdx = binary_search(inputNodes, 0, nodes - 1, inputLinks[i].nodeB);

        addEdge(graph, nodeAIdx, nodeBIdx, inputLinks[i].length);
    }

    dijkstra(graph, 0, dist, parent);
    printArr(0, dist, parent, V);

    //plot entire map
    render(nodes, links, inputNodes, inputLinks);


    //plot only shortest paths from src to 15 random (first sorted 15 nodes)
    render_shortest(0, dist, parent, nodes, links, inputNodes, inputLinks);

    return 0;
}
