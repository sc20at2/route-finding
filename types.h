#ifndef TYPES_H_
# define TYPES_H_

struct Node {
    int id;
    int order_id;
    double lat;
    double lon;
};

struct Link {
    int nodeA;
    int nodeB;
    double length;
};

#endif
